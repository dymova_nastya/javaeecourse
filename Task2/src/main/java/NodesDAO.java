import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openstreetmap.osm._0.Node;

import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.*;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;


public class NodesDAO implements AutoCloseable {
    private static final Logger log = LogManager.getLogger(NodesDAO.class.getName());

    private static final String URL = "jdbc:postgresql://localhost:5433/osm";
    private static final String USER = "postgres";
    private static final String PASSWORD = "1";
    private Connection connection;

    public NodesDAO() throws SQLException, ClassNotFoundException {
        connection = DriverManager.getConnection(URL, USER, PASSWORD);
        Class.forName("org.postgresql.Driver");
    }

    public void addNodeByExecuteUpdate(List<Node> nodes) throws SQLException, ClassNotFoundException {
        try (Statement statement = connection.createStatement()) {
            long before = System.currentTimeMillis();
            int count = 0;
            for (Node node : nodes) {
                String parameters = String.join(", ",
                        String.valueOf(node.getId()),
                        String.valueOf(node.getLat()),
                        String.valueOf(node.getLon()),
                        "'" + node.getUser() + "'",
                        String.valueOf(node.getUid()),
                        String.valueOf(node.isVisible()),
                        String.valueOf(node.getVersion()),
                        String.valueOf(node.getChangeset()),
                        "'" + node.getTimestamp() + "'");

                String request = "INSERT INTO nodes " +
                        "(node_id, node_lat, node_lon, node_user, node_uid, " +
                        "node_visible, node_version, node_changeset, node_timestamp) " +
                        "VALUES (" + parameters + ")";

                count += statement.executeUpdate(request);
            }
            long after = System.currentTimeMillis();
            log.info("Insert " + count + " records by executeUpdate");
            log.info("Time: " + (after - before) + " ms");
        }

    }

    public void addNodeByPreparedStatement(List<Node> nodes) throws SQLException, ClassNotFoundException {


        String request = "INSERT INTO nodes " +
                "(node_id,node_lat, node_lon, node_user, node_uid, " +
                "node_visible, node_version, node_changeset, node_timestamp) " +
                "VALUES (? , ?, ?, ? , ?, ?, ? , ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(request)) {
            long before = System.currentTimeMillis();
            int count = 0;
            for (Node node : nodes) {
                statement.setObject(1, node.getId(), Types.BIGINT);
                statement.setDouble(2, node.getLat());
                statement.setDouble(3, node.getLon());
                statement.setString(4, node.getUser());
                statement.setObject(5, node.getUid(), Types.BIGINT);
                if (node.isVisible() == null) {
                    statement.setNull(6, Types.BOOLEAN);
                } else {
                    statement.setBoolean(6, node.isVisible());
                }
                statement.setObject(7, node.getVersion(), Types.BIGINT);
                statement.setObject(8, node.getChangeset(), Types.BIGINT);

                XMLGregorianCalendar calendar = node.getTimestamp();
                GregorianCalendar gregorianCalendar = calendar.toGregorianCalendar();
                Timestamp timestamp = new Timestamp(gregorianCalendar.getTimeInMillis());
                statement.setTimestamp(9, timestamp);

                count += statement.executeUpdate();
            }
            long after = System.currentTimeMillis();
            log.info("Insert " + count + " records by PreparedStatement");
            log.info("Time: " + (after - before) + " ms");
        }

    }

    public void addNodeByBatch(List<Node> nodes) throws SQLException, ClassNotFoundException {
        connection.setAutoCommit(false);

        try (Statement statement = connection.createStatement()) {
            long before = System.currentTimeMillis();
            for (Node node : nodes) {
                String parameters = String.join(", ",
                        String.valueOf(node.getId()),
                        String.valueOf(node.getLat()),
                        String.valueOf(node.getLon()),
                        "'" + node.getUser() + "'",
                        String.valueOf(node.getUid()),
                        String.valueOf(node.isVisible()),
                        String.valueOf(node.getVersion()),
                        String.valueOf(node.getChangeset()),
                        "'" + node.getTimestamp() + "'");
                String request = "INSERT INTO nodes " +
                        "(node_id, node_lat, node_lon, node_user, node_uid, " +
                        "node_visible, node_version, node_changeset, node_timestamp) " +
                        "VALUES (" + parameters + ")";
                statement.addBatch(request);
            }
            int[] results = statement.executeBatch();
            connection.commit();
            long after = System.currentTimeMillis();
            log.info("Insert " + Arrays.stream(results).count() + " records by Batch");
            log.info("Time: " + (after - before) + " ms");
        }

    }

    @Override
    public void close() throws Exception {
        connection.close();
    }


}
