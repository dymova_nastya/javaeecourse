import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import javax.xml.stream.*;
import java.io.*;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {

        try(FileInputStream fin = new FileInputStream(Paths.get("src", "main", "resources", "RU-NVS.osm.bz2").toString());
            BufferedInputStream in = new BufferedInputStream(fin);
            BZip2CompressorInputStream bzIn = new BZip2CompressorInputStream(in)) {

            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            XMLEventReader reader = inputFactory.createXMLEventReader(bzIn);

            StatisticsCounter counter = new StatisticsCounter();
            counter.process(reader);


        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }

    }
}
